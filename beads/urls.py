from django.urls import path
from . import views

urlpatterns = [
   path('', views.account_new, name='account_new'),
   path('users/detail/<pk>/', views.account_detail, name='account_detail'),
   #path('users/signin/<pk>/', views.account_save, name='account_save'),
   path('users/signin/', views.account_save, name='account_save'),
]
