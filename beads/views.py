from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect, HttpResponse

from .forms import UserForm, LoginForm
from .models import User
# Create your views here.


def account_detail(request, pk):
    """
    Displays general user information.
    """
    user = get_object_or_404(User, pk=pk)
    return render(request, 'beads/user_detail.html', {'user': user})


def account_save(request):
    """
    Show template signIn .
    When button signIn is clicked , check if the user exists or not in the database. 
    If user exists in the database (if pk is in database) : show template user detail.
    If user not exists in the database :  message error. 
    """
    if  request.method=="POST":
        form= LoginForm(request.POST)
        box_number = request.POST.get('box_number', '')
        password = request.POST.get('password', '')
        user_exists = User.objects.filter(box_number=box_number, password=password).count()==1 
        if user_exists :
            return redirect('account_detail', pk=box_number)
        else:
            return redirect('account_new') # need to validate
    else:
        form = LoginForm()

    return render(request, 'beads/user_singin.html', {'form': form})



def account_new(request):
    """
    Shows the form to fill the data of the document submission statement.
    When the register button is clicked it redirects you to the sign in page.
    """
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.save()
            return redirect('account_save')
    else:
        form = UserForm()
    return render(request, 'beads/form_before_account.html', {'form': form})
