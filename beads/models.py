from django.db import models
from django.utils import timezone
# Formulario para la gente de HD, Cuando llega el documento submission statement , HD llena los campos

class User(models.Model):
    box_number= models.CharField(max_length=12,primary_key=True,unique=True) #  (ega-box-XXXX)
    first_name = models.CharField(max_length=50)
    last_name= models.CharField(max_length=50)
    email =  models.EmailField(max_length=70)# tienen que haber infinitos inputs email, un más para que pueda crear otro
    password= models.CharField(max_length=40)
    rt_ticket =  models.CharField(max_length=30) # (ticket en Helpdesk system)
    submission_type =  models.CharField(max_length=30) # select  (Sequence, Array, Phenotype)
    region =  models.CharField(max_length=30)  #//que salgan todos los paises y un autocompletado Europa
    country = models.CharField(max_length=30)  # que salgan los paises de esa region, y un autocompletado
    institute_name = models.CharField(max_length=80)
    account_accession = models.CharField(max_length=14)  # (EGABXXXXXXXXXX), intentar que sea automático
    center_name = models.CharField(max_length=80)
    dta =  models.BooleanField() # checkbox , no es una pregunta
    comment = models.TextField() # textarea
    created_date = models.DateTimeField(default=timezone.now)  


    def __str__(self):
        return '%s %s' % (self.email, self.box_number)

    