from django import forms

from .models import User

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields=('__all__')
        exclude = ['created_date']

class LoginForm(forms.ModelForm):
    class Meta:
        model=User
        fields =('box_number','password')